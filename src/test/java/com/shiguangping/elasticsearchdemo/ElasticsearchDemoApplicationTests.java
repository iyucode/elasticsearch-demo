package com.shiguangping.elasticsearchdemo;

import com.alibaba.fastjson.JSON;
import com.shiguangping.elasticsearchdemo.config.ElasticSearchConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.IndicesClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class ElasticsearchDemoApplicationTests {

    @Autowired
    private RestHighLevelClient client;

    @SneakyThrows
    @Test
    public void index(){
        // 1. 创建IndexRequest实例，指定索引
        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1"); // 设置id

        User user = new User("李达康", 45, "男");
        // 2. 将要保存到ES中的对象转为Json字符串存进去
        indexRequest.source(JSON.toJSONString(user), XContentType.JSON);

        // 3. 调用index()方法执行保存操作，这里用到了配置类中定义的COMMON_OPTIONS
        IndexResponse indexResponse = client.index(indexRequest, ElasticSearchConfig.COMMON_OPTIONS);

        System.out.println(JSON.toJSONString(indexResponse));
    }

}

@Data
@AllArgsConstructor
class User {
    private String name;
    private Integer age;
    private String gender;
}
